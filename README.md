# rustclue

WIP Rust port of the geoclue service. Composed of a library and a binary that makes use of it.

- Library documentation: <https://bilelmoussaoui.pages.freedesktop.org/rustclue/rustclue/>
