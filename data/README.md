# How to setup
- `sudo ostree admin unlock`
- `sudo cp data/org.freedesktop.GeoClue3.conf /usr/share/dbus-1/system.d/`
- `sudo cp data/org.freedesktop.GeoClue3.service /usr/share/dbus-1/system-services/`
- `sudo cp data/geoclue2.service /usr/lib/systemd/system`