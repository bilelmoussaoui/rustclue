use std::sync::Arc;

use event_listener::{Event, EventListener};

use crate::{location::Location, sources::web, Result};

/// The main entry of the location service
/// It starts the various sources based on what have been configured
/// at build time and monitors the requests from the end-user applications
/// to provide them with the potentially cached locations.
#[derive(Debug)]
pub struct Locator {
    web_source: web::WebSource,
    location_updated: Arc<Event>,
    location: Option<Location>,
}

impl Locator {
    pub async fn new(cnx: &zbus::Connection) -> Result<Self> {
        let web_source = web::WebSource::new(cnx).await?;

        Ok(Self {
            web_source,
            location_updated: Arc::new(Event::new()),
            location: None,
        })
    }

    pub fn location_updated(&self) -> EventListener {
        self.location_updated.listen()
    }

    pub fn location(&mut self) -> Option<Location> {
        self.location.take()
    }

    pub async fn start(&self) -> Result<()> {
        // TODO: check if the requested accuracty < available source accuracy
        self.web_source.start().await?;
        Ok(())
    }

    pub async fn stop(&self) -> Result<()> {
        self.web_source.stop().await?;
        Ok(())
    }

    pub async fn refresh(&mut self) -> Result<()> {
        let location = self.web_source.refresh().await?;
        tracing::debug!("Obtained location {:#?}", location);
        self.set_location(location);
        self.location_updated.notify(usize::MAX); // There would be only one listener, eg the client
        Ok(())
    }

    fn set_location(&mut self, location: Location) {
        self.location = Some(location);
    }
}
