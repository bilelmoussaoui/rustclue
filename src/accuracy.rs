use serde::{Deserialize, Serialize};
use std::fmt;
use zbus::zvariant::{OwnedValue, Type, Value};

#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Serialize,
    Deserialize,
    Type,
    OwnedValue,
    Value,
)]
#[zvariant(signature = "u")]
/// The [`Location`][crate::location::Location] accuracy.
pub enum Accuracy {
    ///  Accuracy level unknown or unset.
    None = 0,
    ///  Country-level accuracy.
    Country = 1,
    /// City-level accuracy.
    City = 4,
    ///  Neighborhood-level accuracy.
    Neighborhood = 5,
    ///  Street-level accuracy.
    Street = 6,
    ///  Exact accuracy. Typically requires GPS receiver.
    Exact = 8,
}

impl fmt::Display for Accuracy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::None => f.write_str("None"),
            Self::Country => f.write_str("Country"),
            Self::City => f.write_str("City"),
            Self::Neighborhood => f.write_str("Neighborhood"),
            Self::Street => f.write_str("Street"),
            Self::Exact => f.write_str("Exact"),
        }
    }
}

impl From<&zbus::zvariant::Value<'_>> for Accuracy {
    fn from(value: &zbus::zvariant::Value<'_>) -> Self {
        Self::try_from(value).unwrap()
    }
}

impl Default for Accuracy {
    fn default() -> Self {
        Self::Exact
    }
}
