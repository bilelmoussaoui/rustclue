use zbus::{
    dbus_proxy,
    fdo::Result,
    zvariant::{ObjectPath, OwnedObjectPath},
};

use crate::accuracy::Accuracy;

#[dbus_proxy(
    interface = "org.freedesktop.GeoClue3.Client",
    default_service = "org.freedesktop.GeoClue3",
    default_path = "/org/freedesktop/GeoClue3/Client",
    gen_blocking = false,
    async_name = "ClientProxy"
)]
pub trait Client {
    #[dbus_proxy(property)]
    fn location(&self) -> Result<OwnedObjectPath>;

    #[dbus_proxy(property)]
    fn distance_threshold(&self) -> Result<u32>;

    #[dbus_proxy(property)]
    fn set_distance_threshold(&self, distance_threshold: u32) -> Result<()>;

    #[dbus_proxy(property)]
    fn time_threshold(&self) -> Result<u32>;

    #[dbus_proxy(property)]
    fn set_time_threshold(&self, time_threshold: u32) -> Result<()>;

    #[dbus_proxy(property)]
    fn desktop_id(&self) -> Result<String>;

    #[dbus_proxy(property)]
    fn set_desktop_id(&self, desktop_id: &str) -> Result<()>;

    #[dbus_proxy(property)]
    fn requested_accuracy_level(&self) -> Result<Accuracy>;

    #[dbus_proxy(property)]
    fn set_requested_accuracy_level(&self, requested_accuracy_level: Accuracy) -> Result<()>;

    #[dbus_proxy(property)]
    fn active(&self) -> Result<bool>;

    fn start(&self) -> Result<()>;

    fn stop(&self) -> Result<()>;

    #[dbus_proxy(signal)]
    fn location_updated(&self, old: ObjectPath<'_>, new: ObjectPath<'_>) -> Result<()>;
}
