use std::str::Lines;

use crate::{Error, Result};
use tokio::io::AsyncReadExt;
use zbus::names::{BusName, OwnedBusName};

use super::client::UserId;

#[derive(Clone, Debug)]
pub struct ClientInfo {
    user_id: UserId,
    proccess_id: u32,
    xdg_id: Option<String>,
    // TODO: use a UniqueName<'some_lifetime>
    bus_name: OwnedBusName,
}

impl ClientInfo {
    pub async fn new(bus_name: BusName<'_>, cnx: &zbus::Connection) -> Result<Self> {
        let proxy = zbus::fdo::DBusProxy::new(cnx).await?;
        let user_id = proxy.get_connection_unix_user(bus_name.clone()).await?;
        let proccess_id = proxy
            .get_connection_unix_process_id(bus_name.clone())
            .await?;
        let xdg_id = xdg_id(proccess_id)
            .await
            .map_err(|_e| Error::IO("Failed to retrieve the xdg-id"))?;
        Ok(Self {
            user_id,
            xdg_id,
            proccess_id,
            bus_name: bus_name.into(),
        })
    }

    pub fn user_id(&self) -> UserId {
        self.user_id
    }

    pub fn proccess_id(&self) -> u32 {
        self.proccess_id
    }

    pub fn bus_name(&self) -> &BusName<'_> {
        &self.bus_name
    }

    pub fn xdg_id(&self) -> Option<&str> {
        self.xdg_id.as_deref()
    }
}

// TODO: check if this should ever be nullable
async fn xdg_id(pid: u32) -> std::result::Result<Option<String>, std::io::Error> {
    let path = format!("/proc/{}/cgroup", pid);
    let mut file = tokio::fs::File::open(path).await?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;
    if buffer.is_empty() {
        return Ok(None);
    }

    let c_groupe = parse_cgroup_v2(buffer.lines());

    if c_groupe.is_some() {
        return Ok(c_groupe.map(|c| c.to_owned()));
    }

    let mut xdg_id = None;
    for line in buffer.lines() {
        let unit = line.trim_start_matches("1:name=systemd:");
        if !line.starts_with("1:name=systemd:") {
            continue;
        }
        let scope = std::path::Path::new(unit)
            .file_name()
            .map(|f| f.to_str())
            .flatten()
            .unwrap_or_default();
        let prefix = if scope.starts_with("xdg-app-") {
            Some("xdg-app")
        } else if scope.starts_with("flatpak-") {
            Some("flatpak-")
        } else if scope.starts_with("app-flatpak-") {
            Some("app-flatpak-")
        } else {
            None
        };

        if prefix.is_none() || !scope.ends_with(".scope") {
            break;
        }
        let name = scope.trim_start_matches(prefix.unwrap());
        if !name.contains('-') {
            break;
        }

        xdg_id = Some(name);
    }

    Ok(xdg_id.map(|c| c.to_owned()))
}

fn parse_cgroup_v2(lines: Lines) -> Option<&str> {
    let line = lines.into_iter().next()?;
    if !line.starts_with("0::") {
        return None;
    }
    let unit = line.trim_start_matches("0::");
    let scope = std::path::Path::new(unit).file_name()?.to_str()?;
    if !scope.starts_with("app-flatpak") || !scope.ends_with(".scope") {
        return None;
    }
    let name = scope.trim_start_matches("app-flatpak-");
    if !name.contains('-') {
        return None;
    }

    Some(name)
}

#[cfg(test)]
mod tests {
    use super::parse_cgroup_v2;

    #[test]
    fn parse_cgroup_v2_test() {
        let data = "0::/user.slice/user-1000.slice/user@1000.service/app.slice/app-flatpak-org.gnome.Maps-3358.scope";
        assert_eq!(
            parse_cgroup_v2(data.lines()),
            Some("org.gnome.Maps-3358.scope")
        );

        let data = "0::/../../../app.slice/app-flatpak-org.gnome.Polari-59166.scope";
        assert_eq!(
            parse_cgroup_v2(data.lines()),
            Some("org.gnome.Polari-59166.scope")
        );

        let data = "0::/../../../app.slice/vte-spawn-0c1891c4-5ccc-46b4-bbdc-81106b7b4a98.scope";
        assert_eq!(parse_cgroup_v2(data.lines()), None);
    }
}
