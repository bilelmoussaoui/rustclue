use zbus::{dbus_proxy, fdo::Result};

#[dbus_proxy(
    interface = "org.freedesktop.GeoClue3.Location",
    default_service = "org.freedesktop.GeoClue3",
    default_path = "/org/freedesktop/GeoClue3/Location",
    gen_blocking = false,
    async_name = "LocationProxy"
)]
pub trait Location {
    #[dbus_proxy(property)]
    fn latitude(&self) -> Result<f64>;

    #[dbus_proxy(property)]
    fn longitude(&self) -> Result<f64>;

    #[dbus_proxy(property)]
    fn accuracy(&self) -> Result<f64>;

    #[dbus_proxy(property)]
    fn altitude(&self) -> Result<f64>;

    #[dbus_proxy(property)]
    fn speed(&self) -> Result<f64>;

    #[dbus_proxy(property)]
    fn heading(&self) -> Result<f64>;

    #[dbus_proxy(property)]
    fn description(&self) -> Result<String>;

    #[dbus_proxy(property)]
    fn timestamp(&self) -> Result<(u64, u64)>;
}
