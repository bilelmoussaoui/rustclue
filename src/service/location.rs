use zbus::dbus_interface;

/// Wrapper of the generic [`Location`](crate::location::Location) type.
/// that implements the `org.freedesktop.GeoClue2.Location` DBus interface.
pub struct Location(crate::location::Location);

impl Location {
    pub fn new(location: crate::location::Location) -> Self {
        Self(location)
    }
}

#[dbus_interface(name = "org.freedesktop.GeoClue3.Location")]
impl Location {
    #[dbus_interface(property)]
    pub fn latitude(&self) -> f64 {
        self.0.latitude()
    }

    #[dbus_interface(property)]
    pub fn longitude(&self) -> f64 {
        self.0.longitude()
    }

    #[dbus_interface(property)]
    pub fn accuracy(&self) -> f64 {
        self.0.accuracy()
    }

    /// The altitude or `f64::MAX` if unknown.
    #[dbus_interface(property)]
    pub fn altitude(&self) -> f64 {
        self.0.altitude().unwrap_or(f64::MAX)
    }

    /// The speed or -1.0 if unknown.
    #[dbus_interface(property)]
    pub fn speed(&self) -> f64 {
        self.0.speed().unwrap_or(-1.0)
    }

    /// The heading direction or -1.0 if unknown.
    #[dbus_interface(property)]
    pub fn heading(&self) -> f64 {
        self.0.heading().unwrap_or(-1.0)
    }

    /// A human readable description if available
    #[dbus_interface(property)]
    pub fn description(&self) -> &str {
        self.0.description().unwrap_or_default()
    }

    #[dbus_interface(property)]
    pub fn timestamp(&self) -> (u64, u64) {
        let duration = self.0.timestamp();
        (
            duration.as_secs(),
            duration
                .as_micros()
                .try_into()
                .expect("Failed to convert micro seconds to u64"),
        )
    }
}

impl From<crate::location::Location> for Location {
    fn from(l: crate::location::Location) -> Self {
        Self(l)
    }
}
