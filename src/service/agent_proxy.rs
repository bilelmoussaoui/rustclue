use zbus::dbus_proxy;

use crate::accuracy::Accuracy;

#[dbus_proxy(
    interface = "org.freedesktop.GeoClue3.Agent",
    default_service = "org.freedesktop.GeoClue3",
    default_path = "/org/freedesktop/GeoClue3/Agent",
    gen_blocking = false,
    async_name = "AgentProxy"
)]
pub trait Agent {
    fn authorize_app(
        &self,
        desktop_id: &str,
        req_accuracy_level: Accuracy,
    ) -> zbus::fdo::Result<(bool, Accuracy)>;

    #[dbus_proxy(property)]
    fn max_accuracy_level(&self) -> zbus::fdo::Result<Accuracy>;
}
