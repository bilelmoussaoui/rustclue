use std::{
    collections::{HashMap, VecDeque},
    sync::Arc,
};

use event_listener::Event;
use futures::lock::Mutex;
use zbus::{
    dbus_interface,
    names::{BusName, OwnedBusName},
    zvariant::{ObjectPath, OwnedObjectPath},
    MessageHeader,
};

use super::{client::UserId, AgentProxy, Client};
use crate::{
    accuracy::Accuracy,
    config::CONFIG,
    locator::Locator,
    service::{client_info::ClientInfo, CLIENT_PATH, MANAGER_PATH},
    Result,
};

#[derive(Debug, Clone)]
pub struct Manager {
    cnx: zbus::Connection,
    clients: Arc<Mutex<Vec<Client>>>,
    agents: Arc<Mutex<HashMap<UserId, AgentProxy<'static>>>>,
    num_clients: Arc<Mutex<u32>>,
    locator: Arc<Locator>,
    clients_waiting_agent: Arc<Mutex<VecDeque<ClientInfo>>>,
    last_client_id: Arc<Mutex<u32>>,
    is_active: Arc<Mutex<bool>>,
    agent_ready: Arc<Event>,
    in_use: Arc<Mutex<bool>>,
}

impl Manager {
    pub async fn new(cnx: &zbus::Connection) -> Result<Self> {
        let locator = Arc::new(Locator::new(cnx).await?);

        let manager = Self {
            cnx: cnx.clone(),
            clients: Default::default(),
            agents: Default::default(),
            clients_waiting_agent: Default::default(),
            num_clients: Arc::new(Mutex::new(0)),
            last_client_id: Arc::new(Mutex::new(0)),
            locator,
            is_active: Arc::new(Mutex::new(false)),
            agent_ready: Arc::new(Event::new()),
            in_use: Arc::new(Mutex::new(false)),
        };
        Ok(manager)
    }

    pub async fn handle_get_client(
        &self,
        bus_name: BusName<'_>,
        reuse_client: bool,
    ) -> Result<Option<Client>> {
        let client_info = ClientInfo::new(bus_name.clone(), &self.cnx).await?;
        let user_id = client_info.user_id();

        if let Some(agent) = self.agents.lock().await.get(&user_id) {
            tracing::debug!("Found an agent for {user_id}");
            if reuse_client {
                if let Some(client) = self
                    .clients
                    .lock()
                    .await
                    .iter()
                    .find(|c| c.info().bus_name() == &bus_name)
                {
                    tracing::debug!("Reusing the same client");
                    return Ok(Some(client.clone()));
                }
            }
            let client = self
                .on_agent_ready_get_client(agent.clone(), client_info)
                .await?;
            Ok(Some(client))
        } else {
            self.clients_waiting_agent
                .lock()
                .await
                .push_back(client_info);
            tracing::debug!("Client {bus_name} is waiting for agent for {user_id}");
            // The client would be created later once the agent is ready
            Ok(None)
        }
    }

    async fn on_agent_ready_get_client(
        &self,
        agent: AgentProxy<'static>,
        client_info: ClientInfo,
    ) -> Result<Client> {
        let mut last_client_id = self.last_client_id.lock().await;
        *last_client_id += 1;
        let client_path =
            ObjectPath::try_from(format!("{}/{}", CLIENT_PATH, *last_client_id)).unwrap(); // Shouldn't crash
        let client = Client::new(&self.cnx, client_info, client_path, agent).await;
        self.clients.lock().await.insert(0, client.clone());
        self.handle_on_client_active_changed(client.clone());

        let mut num_clients = self.num_clients.lock().await;
        *num_clients += 1;

        if *num_clients == 1 {
            *self.is_active.lock().await = true;
        }
        tracing::debug!("Connected clients {}", *num_clients);
        Ok(client)
    }

    async fn wait_for_agent_to_be_ready(&self, bus_name: OwnedBusName) -> Client {
        tracing::debug!("Waiting for an agent before creating the client");
        let listener = self.agent_ready.listen();
        listener.await;
        let clients = self.clients.lock().await;
        let client = clients.iter().find(|c| c.info().bus_name() == &bus_name);
        client
            .expect("Something unexpected happened, the client should be there")
            .clone()
    }

    async fn handle_delete_client(&self, client: ObjectPath<'_>) -> Result<()> {
        let mut clients = self.clients.lock().await;
        let total_before = clients.len();
        clients.retain(move |c| c.path() != &client);
        let total_after = clients.len();
        if total_before != total_after {
            *self.num_clients.lock().await -= 1;
            self.sync_in_use().await?;
        }
        Ok(())
    }

    fn handle_on_client_active_changed(&self, client: Client) {
        let this = self.clone();
        tokio::spawn(async move {
            loop {
                let listener = client.active_updated();
                listener.await;
                if let Err(err) = this.sync_in_use().await {
                    tracing::error!("Failed to sync in-use property {}", err);
                }
            }
        });
    }

    async fn sync_in_use(&self) -> Result<()> {
        let mut in_use = false;
        let clients = self.clients.lock().await;
        for client in clients.iter() {
            let id = client.desktop_id();
            if client.active().await && !CONFIG.is_system_component(id) {
                in_use = true;
                break;
            }
        }
        if in_use != self.in_use().await {
            *self.in_use.lock().await = in_use;
            // TODO: see if we can pass the signal ctx around
            self.in_use_changed(&zbus::SignalContext::new(&self.cnx, MANAGER_PATH)?)
                .await?;
        }
        Ok(())
    }
}

#[dbus_interface(name = "org.freedesktop.GeoClue3.Manager")]
impl Manager {
    #[dbus_interface(property)]
    pub async fn in_use(&self) -> bool {
        *self.in_use.lock().await
    }

    #[dbus_interface(property)]
    pub fn available_accuracy_level(&self) -> Accuracy {
        Accuracy::None
    }

    pub async fn get_client(
        &self,
        #[zbus(header)] hdr: MessageHeader<'_>,
    ) -> zbus::fdo::Result<OwnedObjectPath> {
        let bus_name = BusName::Unique(hdr.sender()?.expect("Message sender ie empty").clone());
        let client = match self.handle_get_client(bus_name.clone(), true).await {
            Ok(Some(client)) => Ok(client),
            Ok(None) => {
                let client = self.wait_for_agent_to_be_ready(bus_name.into()).await;
                Ok(client)
            }
            Err(err) => Err(zbus::fdo::Error::Failed(format!(
                "Failed to get a client {}",
                err
            ))),
        }?;

        let path = client.path().to_owned();
        let object_server = self.cnx.object_server();
        object_server.at(&path, client).await?;
        Ok(path.into())
    }

    pub async fn create_client(
        &self,
        #[zbus(header)] hdr: MessageHeader<'_>,
    ) -> zbus::fdo::Result<OwnedObjectPath> {
        let bus_name = BusName::Unique(hdr.sender()?.expect("Message sender ie empty").clone());
        let client = match self.handle_get_client(bus_name.clone(), false).await {
            Ok(Some(client)) => Ok(client),
            Ok(None) => {
                let client = self.wait_for_agent_to_be_ready(bus_name.into()).await;
                Ok(client)
            }
            Err(err) => Err(zbus::fdo::Error::Failed(format!(
                "Failed to create a client {}",
                err
            ))),
        }?;

        let path = client.path().to_owned();
        let object_server = self.cnx.object_server();
        object_server.at(&path, client).await?;
        Ok(path.into())
    }

    pub async fn delete_client(&self, client: ObjectPath<'_>) -> zbus::fdo::Result<()> {
        self.handle_delete_client(client)
            .await
            .map_err(|_| zbus::fdo::Error::Failed("Failed to delete client".to_string()))
    }

    pub async fn add_agent(
        &self,
        #[zbus(header)] hdr: MessageHeader<'_>,
        id: &str,
    ) -> zbus::fdo::Result<()> {
        let bus_name = BusName::Unique(hdr.sender()?.expect("Message sender ie empty").clone());
        let client_info = ClientInfo::new(bus_name.clone(), &self.cnx).await.unwrap();
        let user_id = client_info.user_id();

        let agent = AgentProxy::builder(&self.cnx)
            .destination(bus_name.into_owned())?
            .build()
            .await?;
        let xdg_id = client_info.xdg_id();

        if xdg_id.is_some() || !CONFIG.is_agent_allowed(id) {
            return Err(zbus::fdo::Error::Failed(format!(
                "{id} is not allowed to act as an agent"
            )));
        }

        tracing::debug!("New agent {id} for user ID {}", user_id);

        let mut clients_ids_to_drop = vec![];
        let mut clients_waiting_agent = self.clients_waiting_agent.lock().await;
        for client_info in clients_waiting_agent.iter() {
            if client_info.user_id() == user_id {
                tracing::debug!("Creating a client for {}", client_info.user_id());
                let client = self
                    .on_agent_ready_get_client(agent.clone(), client_info.clone())
                    .await
                    .map_err(|_| {
                        zbus::fdo::Error::Failed("Failed to create a client".to_string())
                    })?;
                self.clients.lock().await.insert(0, client.clone());

                clients_ids_to_drop.push(user_id);
            }
        }

        clients_waiting_agent.retain(|e| !clients_ids_to_drop.contains(&e.user_id()));
        self.agent_ready.notify(usize::MAX);
        tracing::debug!("Stored agent '{}' for '{}'", agent.path(), user_id);
        self.agents.lock().await.insert(user_id, agent);
        Ok(())
    }
}
