use crate::sources::web;
use std::time::Duration;

/// A generic location representation that is sent when `location-updated` signal
/// is emitted.
#[derive(Debug, Clone)]
pub struct Location {
    description: Option<String>,
    latitude: f64,
    longitude: f64,
    altitude: Option<f64>,
    accuracy: f64,
    timestamp: Duration,
    speed: Option<f64>,
    heading: Option<f64>,
}

impl Location {
    pub fn new(latitude: f64, longitude: f64, accuracy: f64) -> Self {
        Self {
            latitude,
            longitude,
            accuracy,
            description: None,
            altitude: None,
            timestamp: Duration::from_nanos(0), // TODO: figure out when we should get the timestamp
            speed: None,
            heading: None,
        }
    }

    pub fn description(&self) -> Option<&str> {
        self.description.as_deref()
    }

    pub fn latitude(&self) -> f64 {
        self.latitude
    }

    pub fn longitude(&self) -> f64 {
        self.longitude
    }

    pub fn altitude(&self) -> Option<f64> {
        self.altitude
    }

    pub fn accuracy(&self) -> f64 {
        self.accuracy
    }

    pub fn timestamp(&self) -> Duration {
        self.timestamp
    }

    pub fn speed(&self) -> Option<f64> {
        self.speed
    }

    pub fn heading(&self) -> Option<f64> {
        self.heading
    }

    pub fn duplicate(&self) -> Self {
        let mut duplicated_location = self.clone();
        duplicated_location.set_timestamp(Duration::from_nanos(0));
        duplicated_location
    }

    /// Set the location's timestamp.
    pub fn set_timestamp(&mut self, timestamp: Duration) {
        self.timestamp = timestamp;
    }
}

impl From<web::mozilla::Location> for Location {
    fn from(loc: web::mozilla::Location) -> Self {
        Self::new(loc.latitude(), loc.longitude(), loc.accuracy())
    }
}
