use futures::lock::Mutex;
use std::{collections::HashMap, sync::Arc};
use zbus::{export::futures_util::StreamExt, zvariant::OwnedObjectPath};

use super::mozilla::WiFiAccessPoint;
use crate::{
    accuracy::Accuracy,
    interfaces::wpa_supplicant::{
        ScanArgs, WPASupplicantBSSProxy, WPASupplicantInterfaceProxy, WPASupplicantProxy,
        WIFI_SCAN_BSS_NOISE_LEVEL,
    },
    location::Location,
    Result,
};

pub type CacheKey = String;

#[derive(Debug)]
/// WiFi based web source.
///
/// The [Mozilla Location Service](https://location.services.mozilla.com/) supports geolocation based
/// on the available WiFi access points that are fetched using the `fi.w1.wpa_supplicant1` DBus interface.
pub struct WifiSource {
    connection: zbus::Connection,
    proxy: WPASupplicantProxy<'static>,
    interfaces: Vec<WPASupplicantInterfaceProxy<'static>>,
    ignored_bss_proxies: Arc<Mutex<HashMap<OwnedObjectPath, WPASupplicantBSSProxy<'static>>>>,
    bss_proxies: Arc<Mutex<HashMap<OwnedObjectPath, WPASupplicantBSSProxy<'static>>>>,
    cached_locations: Arc<Mutex<HashMap<CacheKey, Location>>>,
    cache_miss: Arc<Mutex<u32>>,
    cache_hit: Arc<Mutex<u32>>,
    accuracy_level: Arc<Mutex<Accuracy>>,
}

impl WifiSource {
    pub async fn new(cnx: &zbus::Connection) -> Result<WifiSource> {
        let proxy = WPASupplicantProxy::new(cnx).await?;
        let mut interfaces = vec![];
        for iface_path in proxy.interfaces().await? {
            interfaces.push(
                WPASupplicantInterfaceProxy::builder(cnx)
                    .path(iface_path)?
                    .build()
                    .await?,
            );
        }
        Ok(Self {
            connection: cnx.clone(),
            proxy,
            interfaces,
            bss_proxies: Default::default(),
            ignored_bss_proxies: Default::default(),
            cached_locations: Default::default(),
            cache_miss: Arc::new(Mutex::new(0)),
            cache_hit: Arc::new(Mutex::new(0)),
            accuracy_level: Arc::new(Mutex::new(Accuracy::City)),
        })
    }

    /// Scan for available WiFi pounts and connect to their corresponding signals
    pub async fn connect_bss_signals(&self) -> Result<()> {
        self.scan_wifi().await?;
        Ok(())
    }

    pub async fn scan_wifi(&self) -> Result<()> {
        for interface in self.interfaces.iter() {
            // start scanning
            interface.scan(ScanArgs::new("passive")).await?;
            // wait until the scanning is done
            let scan_done_stream = interface.receive_scan_done().await?.next().await.unwrap();
            if !scan_done_stream.args()?.success {
                // TODO: log that the scanning failed for this interface
            } else {
                let bss_list = interface.bss_list().await?;
                for bss_path in bss_list.into_iter() {
                    match WPASupplicantBSSProxy::builder(&self.connection)
                        .path(bss_path.clone())?
                        .build()
                        .await
                    {
                        Ok(bss_iface) => {
                            self.on_bss_ready(bss_path, bss_iface).await?;
                        }
                        Err(err) => {
                            // TODO: log the error
                        }
                    }
                }
            }
        }
        Ok(())
    }

    pub async fn on_bss_ready(
        &self,
        bss_path: OwnedObjectPath,
        bss_iface: WPASupplicantBSSProxy<'static>,
    ) -> Result<()> {
        let ssid = bss_iface.ssid().await?;
        tracing::debug!("WiFi AP {ssid} detected");
        let signal = bss_iface.signal().await?;

        if signal <= WIFI_SCAN_BSS_NOISE_LEVEL {
            let bssid = bss_iface.bssid().await?;
            tracing::debug!("WiFi AP {bssid} has very low strength {signal} dBm, ignoring for now");
            // TODO: wait for a modification of the signal property and update the ignored list
        } else {
            self.bss_proxies.lock().await.insert(bss_path, bss_iface);
            tracing::debug!("WiFi AP {ssid} added");
        }
        Ok(())
    }

    /// Generate a cache key based on the detected BSS proxies
    pub async fn cache_key(&self) -> Result<CacheKey> {
        let cached_proxies = self.bss_proxies.lock().await;
        let mut proxies = vec![];
        for (_, proxy) in cached_proxies.iter() {
            let bssid = proxy.bssid().await?;
            if bssid.is_empty() {
                continue;
            }

            let signal = proxy.signal().await? / 10;
            proxies.push((bssid.to_string(), signal));
        }
        proxies.sort_by(|a, b| a.0.cmp(&b.0));
        let cache_key = proxies
            .iter()
            .map(|(bssid, signal)| format!("{bssid}{signal}"))
            .collect::<Vec<_>>()
            .join("");
        Ok(cache_key)
    }

    pub(crate) async fn cache_ratio(&self) -> f64 {
        let cache_hits = self.cache_hit.lock().await;
        let cache_miss = self.cache_miss.lock().await;
        let cache_attempts = *cache_hits + *cache_miss;

        *cache_hits as f64 * 100.0 / cache_attempts as f64
    }

    pub async fn prepare_query(&self) -> Result<Vec<WiFiAccessPoint>> {
        let bss_proxies = self.bss_proxies.lock().await;
        let access_points = bss_proxies.values();

        let mut points = vec![];
        for point in access_points.cloned() {
            if !point.should_ignore().await? {
                points.push(WiFiAccessPoint::from_proxy(point).await?);
            }
        }

        Ok(points)
    }

    /*
    fn refresh(&self) -> Pin<Box<dyn Future<Output = Result<()>> + 'static>> {
        let this = self.clone();
        let fut = async move {
            let cache_key = this.cache_key().await?;
            let mut cached_locations = this.cached_locations.lock().await;
            if this.is_active() {
                if let Some(cached_location) = cached_locations.get(&cache_key) {
                    *this.cache_hit.lock().await += 1;
                    let new_location = cached_location.duplicate();
                    this.set_location(&new_location)?;
                    let cache_ratio = this.cache_ratio().await;
                    tracing::debug!("Found cache key {cache_key}, hit ratio: {cache_ratio}");
                    return Ok(());
                } else {
                    *this.cache_miss.lock().await += 1;
                    tracing::debug!("Cache miss for {cache_key}, querying web service");
                }
            }
            //let location = WebSourceImpl::refresh(&this).await?;
            cached_locations.insert(cache_key.clone(), location.clone());
            let cache_ratio = this.cache_ratio().await;
            this.set_location(&location)?;
            tracing::debug!("Adding {cache_key} to cache, hit ratio: {cache_ratio} ");
            Ok(())
        };
        Box::pin(fut)
    } */

    async fn available_accuracy_level(&self, is_network_available: bool) -> Accuracy {
        if !is_network_available {
            Accuracy::None
        } else if self.accuracy_level().await != Accuracy::City {
            Accuracy::Street
        } else {
            Accuracy::City
        }
    }

    pub async fn start(&self) -> Result<()> {
        self.connect_bss_signals().await?;
        //WebSource::refresh(&this).await?; // TODO: figure out when we are supposed to call refresh
        Ok(())
    }

    pub async fn stop(&self) -> Result<()> {
        todo!()
    }

    fn location(&self) -> Result<Location> {
        todo!()
    }

    fn set_location(&self, location: &Location) -> Result<()> {
        println!("{:#?}", location);
        //todo!()
        Ok(())
    }

    fn is_active(&self) -> bool {
        true
    }

    async fn accuracy_level(&self) -> Accuracy {
        *self.accuracy_level.lock().await
    }

    fn time_threshold(&self) -> std::time::Duration {
        todo!()
    }
}
