use once_cell::sync::Lazy;
use reqwest::Client;

mod bluetooth_source;
pub mod mozilla;
mod web_source;
mod wifi_source;

pub use bluetooth_source::BluetoothSource;
pub use web_source::WebSource;
pub use wifi_source::WifiSource;

/// A [`reqwest::Client`].
pub static CLIENT: Lazy<Client> = Lazy::new(Client::new);
