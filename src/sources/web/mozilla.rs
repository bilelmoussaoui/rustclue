//! Contains common code that sends queries / parses responses
//! from the Mozilla Location Services <https://location.services.mozilla.com/>.

use crate::interfaces::{bluez::DeviceProxy, wpa_supplicant::WPASupplicantBSSProxy};
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Deserialize)]
/// The error type for a failed geolocate request.
pub struct Error {
    error: LocationErrorInner,
}

#[derive(Deserialize)]
struct LocationErrorInner {
    pub(super) code: u16,
    pub(super) message: String,
    #[serde(rename = "errors")]
    _errors: Vec<LocationErrorDetails>,
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct LocationErrorDetails {
    domain: String,
    reason: String,
    message: String,
}

#[derive(Deserialize)]
#[serde(untagged)]
/// A response to a location [`Request`].
pub enum Response {
    Ok(Location),
    Err(Error),
}

impl From<Response> for Result<Location, crate::Error> {
    fn from(response: Response) -> Self {
        match response {
            Response::Ok(location) => Ok(location),
            Response::Err(err) => Err(crate::Error::LocationService(
                err.error.code,
                err.error.message,
            )),
        }
    }
}

#[derive(Deserialize)]
struct LocationInner {
    lat: f64,
    lng: f64,
}

/// A sucessful response returned after a location request.
///
/// See <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#response> for details.
#[derive(Deserialize)]
pub struct Location {
    location: LocationInner,
    accuracy: f64,
    // Either ipf or lacf. TODO: figure out if this is useful
    fallback: Option<String>,
}

impl Location {
    pub fn latitude(&self) -> f64 {
        self.location.lat
    }

    pub fn longitude(&self) -> f64 {
        self.location.lng
    }

    pub fn accuracy(&self) -> f64 {
        self.accuracy
    }

    /// `ipf` for IP fallback, or `lacf`.
    ///
    /// See <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#fallback-fields> for details.
    pub fn fallback(&self) -> Option<&str> {
        self.fallback.as_deref()
    }
}

impl fmt::Debug for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Location")
            .field("latitude", &self.latitude())
            .field("longitude", &self.longitude())
            .field("accuracy", &self.accuracy())
            .field("fallback", &self.fallback())
            .finish()
    }
}

#[derive(Debug, Default, Serialize, PartialEq, Deserialize)]
/// Represents a Mozilla Location Service geolocate request.
///
/// See <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#request> for details.
pub struct Request {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    carrier: Option<String>,
    #[serde(
        default,
        rename = "wifiAccessPoints",
        skip_serializing_if = "skip_serializing_if_less_then_two"
    )]
    pub wifi_access_points: Vec<WiFiAccessPoint>,
    #[serde(
        default,
        rename = "bluetoothBeacons",
        skip_serializing_if = "skip_serializing_if_less_then_two"
    )]
    pub bluetooth_beacons: Vec<BluetoothBeacon>,
    #[serde(default, rename = "cellTowers", skip_serializing_if = "Vec::is_empty")]
    pub cell_towers: Vec<CellTower>,
    #[serde(
        default,
        rename = "considerIp",
        skip_serializing_if = "Option::is_none"
    )]
    consider_ip: Option<bool>,
    #[serde(
        default,
        rename = "homeMobileCountryCode",
        skip_serializing_if = "Option::is_none"
    )]
    home_mobile_country_code: Option<u32>,
    #[serde(
        default,
        rename = "homeMobileNetworkCode",
        skip_serializing_if = "Option::is_none"
    )]
    home_mobile_network_code: Option<u32>,
    // gsm, wcdma, or lte.
    #[serde(default, rename = "radioType", skip_serializing_if = "Option::is_none")]
    radio_type: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    fallbacks: Option<FallbacksRequest>,
}

/// Represent a WiFi Access Point.
///
/// Sent as part of the gelocation [`Request`] if a WiFi source is available.
/// See <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#wifi-access-point-fields> for details.
#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct WiFiAccessPoint {
    #[serde(rename = "macAddress")]
    mac_address: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    age: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    channel: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    frequency: Option<u64>,
    #[serde(rename = "signalStrength", skip_serializing_if = "Option::is_none")]
    signal_strength: Option<i16>,
    #[serde(rename = "signalToNoiseRatio", skip_serializing_if = "Option::is_none")]
    signal_to_noise_ratio: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    ssid: Option<String>,
}

impl WiFiAccessPoint {
    pub(super) fn new(mac_address: &str) -> Self {
        Self {
            mac_address: mac_address.to_owned(),
            age: Default::default(),
            channel: Default::default(),
            frequency: Default::default(),
            signal_strength: Default::default(),
            signal_to_noise_ratio: Default::default(),
            ssid: Default::default(),
        }
    }

    pub async fn from_proxy(proxy: WPASupplicantBSSProxy<'_>) -> Result<Self, crate::Error> {
        let bss_id = proxy.bssid().await?;
        let mut access_point = Self::new(&bss_id);

        let signal_strength = proxy.signal().await?;
        access_point.signal_strength = Some(signal_strength);
        Ok(access_point)
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
/// Represent a Bluetooth beacon.
///
/// Sent as part of the gelocation [`Request`] if a Bluetooth source is available.
/// See <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#bluetooth-beacon-fields> for details.
pub struct BluetoothBeacon {
    #[serde(rename = "macAddress")]
    mac_address: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    age: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    name: Option<String>,
    #[serde(rename = "signalStrength", skip_serializing_if = "Option::is_none")]
    signal_strength: Option<i16>,
}

impl BluetoothBeacon {
    pub async fn from_proxy(proxy: DeviceProxy<'_>) -> Result<Self, crate::Error> {
        Ok(Self {
            mac_address: proxy.address().await?,
            signal_strength: proxy.signal().await?,
            name: proxy.name().await?,
            age: None,
        })
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
/// Represent a 3G cell tower.
///
/// Sent as part of the gelocation [`Request`] if a 3G source is available.
/// See <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#cell-tower-fields> for details.
pub struct CellTower {
    // gsm, wcdma, or lte.
    #[serde(rename = "radioType")]
    radio_type: String,
    #[serde(rename = "mobileCountryCode", skip_serializing_if = "Option::is_none")]
    mobile_country_code: Option<u32>,
    #[serde(rename = "mobileNetworkCode", skip_serializing_if = "Option::is_none")]
    mobile_network_code: Option<u32>,
    #[serde(rename = "locationAreaCode", skip_serializing_if = "Option::is_none")]
    location_area_code: Option<u32>,
    #[serde(rename = "cellId", skip_serializing_if = "Option::is_none")]
    cell_id: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    age: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    psc: Option<u64>,
    #[serde(rename = "signalStrength", skip_serializing_if = "Option::is_none")]
    signal_strength: Option<i16>,
    #[serde(rename = "timingAdvance", skip_serializing_if = "Option::is_none")]
    timing_advance: Option<i64>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct FallbacksRequest {
    lacf: bool,
    ipf: bool,
}

/// Per <https://ichnaea.readthedocs.io/en/latest/api/geolocate.html#field-definition>
/// two Bluetooth/WiFi networks are the minimum. No need to serialize otherwise
fn skip_serializing_if_less_then_two<T>(data: &[T]) -> bool {
    data.len() < 2
}

#[cfg(test)]
mod tests {
    use super::{
        BluetoothBeacon, CellTower, FallbacksRequest, Location, Request, Response, WiFiAccessPoint,
    };
    use crate::Error;

    #[test]
    fn test_wifi_access_points() {
        let expected = r#"
        {
            "wifiAccessPoints": [{
                "macAddress": "01:23:45:67:89:ab",
                "signalStrength": -51
            }, {
                "macAddress": "01:23:45:67:89:cd"
            }]
        }
        "#;
        let mut request = Request::default();
        let mut access_point = WiFiAccessPoint::new("01:23:45:67:89:ab");
        access_point.signal_strength = Some(-51);
        request.wifi_access_points.push(access_point);
        let access_point = WiFiAccessPoint::new("01:23:45:67:89:cd");
        request.wifi_access_points.push(access_point);

        assert_eq!(serde_json::from_str::<Request>(&expected).unwrap(), request);
    }

    #[test]
    fn test_cell_tower() {
        let expected = r#"
        {
            "cellTowers": [{
                "radioType": "wcdma",
                "mobileCountryCode": 208,
                "mobileNetworkCode": 1,
                "locationAreaCode": 2,
                "cellId": 1234567,
                "signalStrength": -60
            }]
        }
        "#;
        let mut request = Request::default();
        request.cell_towers.push(CellTower {
            radio_type: "wcdma".to_owned(),
            mobile_country_code: Some(208),
            mobile_network_code: Some(1),
            location_area_code: Some(2),
            cell_id: Some(1234567),
            signal_strength: Some(-60),
            age: None,
            psc: None,
            timing_advance: None,
        });
        assert_eq!(serde_json::from_str::<Request>(&expected).unwrap(), request);
    }

    #[test]
    fn test_complete_request() {
        let expected = r#"{
            "carrier": "Telecom",
            "considerIp": true,
            "homeMobileCountryCode": 208,
            "homeMobileNetworkCode": 1,
            "bluetoothBeacons": [{
                "macAddress": "ff:23:45:67:89:ab",
                "age": 2000,
                "name": "beacon",
                "signalStrength": -110
            }],
            "cellTowers": [{
                "radioType": "wcdma",
                "mobileCountryCode": 208,
                "mobileNetworkCode": 1,
                "locationAreaCode": 2,
                "cellId": 1234567,
                "age": 1,
                "psc": 3,
                "signalStrength": -60,
                "timingAdvance": 1
            }],
            "wifiAccessPoints": [{
                "macAddress": "01:23:45:67:89:ab",
                "age": 3,
                "channel": 11,
                "frequency": 2412,
                "signalStrength": -51,
                "signalToNoiseRatio": 13
            }, {
                "macAddress": "01:23:45:67:89:cd"
            }],
            "fallbacks": {
                "lacf": true,
                "ipf": true
            }
        }"#;
        let mut request = Request::default();

        request.carrier = Some("Telecom".to_owned());
        request.consider_ip = Some(true);
        request.home_mobile_country_code = Some(208);
        request.home_mobile_network_code = Some(1);
        request.cell_towers.push(CellTower {
            radio_type: "wcdma".to_owned(),
            mobile_country_code: Some(208),
            mobile_network_code: Some(1),
            location_area_code: Some(2),
            cell_id: Some(1234567),
            signal_strength: Some(-60),
            age: Some(1),
            psc: Some(3),
            timing_advance: Some(1),
        });

        request.bluetooth_beacons.push(BluetoothBeacon {
            mac_address: "ff:23:45:67:89:ab".to_owned(),
            age: Some(2000),
            name: Some("beacon".to_owned()),
            signal_strength: Some(-110),
        });

        request.wifi_access_points.push(WiFiAccessPoint {
            mac_address: "01:23:45:67:89:ab".to_owned(),
            age: Some(3),
            channel: Some(11),
            frequency: Some(2412),
            signal_strength: Some(-51),
            signal_to_noise_ratio: Some(13),
            ssid: None,
        });
        request
            .wifi_access_points
            .push(WiFiAccessPoint::new("01:23:45:67:89:cd"));
        request.fallbacks = Some(FallbacksRequest {
            ipf: true,
            lacf: true,
        });
        assert_eq!(serde_json::from_str::<Request>(&expected).unwrap(), request);
    }

    #[test]
    fn test_deserialize_ok_response() {
        let data = r#"
        {
            "location": {
                "lat": 51.0,
                "lng": -0.1
            },
            "accuracy": 600000.0,
            "fallback": "ipf"
        }
        "#;
        let response = serde_json::from_str::<Response>(data).unwrap();
        let location = <Result<Location, Error>>::from(response).unwrap();
        assert_eq!(location.latitude(), 51.0);
        assert_eq!(location.longitude(), -0.1);
        assert_eq!(location.accuracy(), 600000.0);
        assert_eq!(location.fallback(), Some("ipf"));

        let data = r#"
        {
            "location": {
                "lat": -22.7539192,
                "lng": -43.4371081
            },
            "accuracy": 100.0
        }
        "#;
        let response = serde_json::from_str::<Response>(data).unwrap();
        let location = <Result<Location, Error>>::from(response).unwrap();
        assert_eq!(location.latitude(), -22.7539192);
        assert_eq!(location.longitude(), -43.4371081);
        assert_eq!(location.accuracy(), 100.0);
        assert_eq!(location.fallback(), None);
    }

    #[test]
    fn test_deserialize_error_response() {
        let data = r#"{
            "error": {
                "errors": [{
                    "domain": "geolocation",
                    "reason": "notFound",
                    "message": "Not found"
                }],
                "code": 404,
                "message": "Not found"
            }
        }"#;

        let response = serde_json::from_str::<Response>(data).unwrap();
        let error = <Result<Location, Error>>::from(response);
        assert!(error.is_err());
        let error = error.unwrap_err();
        if let Error::LocationService(code, expected_message) = error {
            assert_eq!(code, 404);
            assert_eq!(expected_message, "Not found".to_owned());
        } else {
            panic!("Unexpected error type");
        }
    }
}
