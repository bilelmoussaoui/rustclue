use super::{mozilla, BluetoothSource, WifiSource, CLIENT};
use crate::{config::CONFIG, location::Location, Result};

// TODO: investigate porting to the V2 of the geosubmit API

/// Check whether a network is available
async fn is_network_available() -> Result<bool> {
    let cnx = zbus::Connection::system().await?;
    let proxy: zbus::Proxy = zbus::ProxyBuilder::new_bare(&cnx)
        .path("/org/freedesktop/NetworkManager")?
        .interface("org.freedesktop.NetworkManager")?
        .destination("org.freedesktop.NetworkManager")?
        .build()
        .await?;
    let connectivity = proxy.get_property::<u32>("Connectivity").await?;
    Ok(connectivity == 4)
}
#[derive(Debug)]
/// The geolocation requests handled by web sources, submits the various
/// information like WiFi acess points, bluetooth beacons, 3G cell towers to a
/// geolocation service (eg, Mozilla Location Service).
///
/// As submitting those information seperately could cause a less accurate geolocation,
/// the WebSource, combines the data from the [`WifiSource`], [`BluetoothSource`]
/// and calls their respective [`WifiSource::prepare_query()`][WifiSource::prepare_query]
/// [`BluetoothSource::prepare_query()`][BluetoothSource::prepare_query] and
/// submits all those information at once.
pub struct WebSource {
    wifi_source: WifiSource,
    bluetooth_source: BluetoothSource,
}

impl WebSource {
    pub async fn new(cnx: &zbus::Connection) -> Result<Self> {
        let bluetooth_source = BluetoothSource::new(cnx).await?;
        let wifi_source = WifiSource::new(cnx).await?;
        Ok(Self {
            bluetooth_source,
            wifi_source,
        })
    }

    pub async fn start(&self) -> Result<()> {
        self.wifi_source.start().await?;
        self.bluetooth_source.start().await?;
        Ok(())
    }

    pub async fn stop(&self) -> Result<()> {
        self.wifi_source.stop().await?;
        self.bluetooth_source.stop().await?;
        Ok(())
    }

    pub async fn refresh(&self) -> Result<Location> {
        let access_points = self.wifi_source.prepare_query().await?;
        let bluetooth_beacons = self.bluetooth_source.prepare_query().await?;

        let mut request = mozilla::Request::default();
        request.bluetooth_beacons = bluetooth_beacons;
        request.wifi_access_points = access_points;
        tracing::debug!("Sending a geolocate request to MLS");
        let response = CLIENT
            .post(CONFIG.url().clone())
            .json(&request)
            .send()
            .await?
            .json::<mozilla::Response>()
            .await?;

        let location = Location::from(<Result<mozilla::Location>>::from(response)?);
        Ok(location)
    }
}
