use crate::{interfaces::bluez::DeviceProxy, Result};
use futures::lock::Mutex;
use std::{collections::HashMap, sync::Arc};
use zbus::zvariant::OwnedObjectPath;

use super::mozilla::BluetoothBeacon;

static BLUEZ_SERVICE: &str = "org.bluez";

#[derive(Debug)]
/// Bluetooth based web source.
///
/// The [Mozilla Location Service](https://location.services.mozilla.com/) supports geolocation based
/// on the available bluetooth beacons that are fetched using [bluez](https://www.bluez.org) DBus API.
pub struct BluetoothSource {
    devices_proxies: Arc<Mutex<HashMap<OwnedObjectPath, DeviceProxy<'static>>>>,
    object_manager: zbus::fdo::ObjectManagerProxy<'static>,
    cnx: zbus::Connection,
}

impl BluetoothSource {
    pub async fn new(cnx: &zbus::Connection) -> Result<Self> {
        let object_manager = zbus::fdo::ObjectManagerProxy::builder(cnx)
            .destination(BLUEZ_SERVICE)?
            .path("/")?
            .build()
            .await?;

        Ok(Self {
            devices_proxies: Default::default(),
            cnx: cnx.clone(),
            object_manager,
        })
    }

    pub async fn connect_signals(&self) -> Result<()> {
        let objects = self.object_manager.get_managed_objects().await?;
        let mut devices_proxies = self.devices_proxies.lock().await;

        for (object_path, map) in objects.into_iter() {
            if map.contains_key("org.bluez.Device1") {
                let device = DeviceProxy::builder(&self.cnx)
                    .path(object_path.clone())?
                    .build()
                    .await?;
                tracing::debug!(
                    "Detected bluetooth device {:#?} with address {}",
                    device.name().await?,
                    device.address().await?
                );
                devices_proxies.insert(object_path, device);
            }
        }
        //TODO: watch for interface-added/interface-removed
        Ok(())
    }

    pub async fn prepare_query(&self) -> Result<Vec<BluetoothBeacon>> {
        let devices_proxies = self.devices_proxies.lock().await;
        let beacons = devices_proxies.values();

        let mut query_params = vec![];
        for beacon in beacons.cloned() {
            if !beacon.should_ignore().await? {
                query_params.push(BluetoothBeacon::from_proxy(beacon).await?);
            }
        }

        Ok(query_params)
    }

    pub async fn start(&self) -> Result<()> {
        self.connect_signals().await?;
        Ok(())
    }

    pub async fn stop(&self) -> Result<()> {
        todo!()
    }

    fn location(&self) -> Result<crate::location::Location> {
        todo!()
    }

    fn set_location(&self, location: &crate::location::Location) -> Result<()> {
        todo!()
    }

    fn is_active(&self) -> bool {
        // TODO: this should check if there is a bluez service around
        true
    }

    async fn accuracy_level(&self) -> crate::accuracy::Accuracy {
        todo!()
    }

    fn time_threshold(&self) -> std::time::Duration {
        todo!()
    }
}
