//! Wrappers for `fi.w1.wpa_supplicant1` DBus interface used by [`WifiSource`](crate::sources::web::WifiSource).

use serde::{de::Visitor, Deserialize};
use std::collections::HashMap;
use std::fmt;
use zbus::{
    dbus_proxy,
    zvariant::{self, ObjectPath, OwnedObjectPath, OwnedValue, SerializeDict, Type, Value},
};

use crate::Result;

pub static WIFI_SCAN_BSS_NOISE_LEVEL: i16 = -90;
static BSSID_LEN: usize = 6;

#[dbus_proxy(
    interface = "fi.w1.wpa_supplicant1",
    default_path = "/fi/w1/wpa_supplicant1",
    gen_blocking = false,
    async_name = "WPASupplicantProxy"
)]
trait WPASupplicant {
    #[dbus_proxy(signal)]
    fn interface_added(&self, path: ObjectPath<'_>, properties: HashMap<&str, Value<'_>>);

    #[dbus_proxy(signal)]
    fn interface_removed(&self, path: ObjectPath<'_>);

    #[dbus_proxy(property)]
    fn interfaces(&self) -> zbus::Result<Vec<OwnedObjectPath>>;
}

#[derive(SerializeDict, Type)]
#[zvariant(signature = "dict")]
pub struct ScanArgs {
    #[zvariant(rename = "Type")]
    type_: String,
}

impl ScanArgs {
    pub fn new(type_: &str) -> Self {
        Self {
            type_: type_.to_owned(),
        }
    }
}

#[dbus_proxy(
    interface = "fi.w1.wpa_supplicant1.Interface",
    default_service = "fi.w1.wpa_supplicant1",
    gen_blocking = false,
    async_name = "WPASupplicantInterfaceProxy"
)]
trait WPASupplicantInterface {
    fn scan(&self, args: ScanArgs) -> zbus::Result<()>;

    #[dbus_proxy(signal, name = "BSSAdded")]
    fn bss_added(&self, path: ObjectPath<'_>, properties: HashMap<&str, Value<'_>>);

    #[dbus_proxy(signal, name = "BSSRemoved")]
    fn bss_removed(&self, path: ObjectPath<'_>);

    #[dbus_proxy(signal)]
    fn scan_done(&self, success: bool);

    #[dbus_proxy(property, name = "Ifname")]
    fn interface_name(&self) -> zbus::Result<String>;

    #[dbus_proxy(property, name = "BSSs")]
    fn bss_list(&self) -> zbus::Result<Vec<OwnedObjectPath>>;
}

#[derive(Debug, PartialEq)]
#[allow(clippy::upper_case_acronyms)]
pub struct SSID(String);

impl SSID {
    pub fn from_bytes_unchecked(v: Vec<u8>) -> Self {
        let mut str = String::new();
        for i in 0..v.len() {
            if let Some(b) = v.get(i) {
                str.push(std::char::from_u32(*b as u32).unwrap());
            }
        }
        Self(str)
    }
}

impl std::ops::Deref for SSID {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Type for SSID {
    fn signature() -> zvariant::Signature<'static> {
        zvariant::Signature::from_static_str_unchecked("ay")
    }
}
#[derive(Default)]

struct SSIDVisitor {}

impl<'de> Visitor<'de> for SSIDVisitor {
    type Value = SSID;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "expecting a [u8] slice")
    }

    fn visit_byte_buf<E>(self, v: Vec<u8>) -> std::result::Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::Value::from_bytes_unchecked(v))
    }
}

impl<'de> Deserialize<'de> for SSID {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let visitor = SSIDVisitor::default();
        let val = deserializer.deserialize_byte_buf(visitor)?;
        Ok(val)
    }
}

impl From<OwnedValue> for SSID {
    fn from(v: OwnedValue) -> Self {
        Self::from_bytes_unchecked(<Vec<u8>>::try_from(v).unwrap())
    }
}

impl fmt::Display for SSID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.0)
    }
}

#[derive(Debug, PartialEq)]
#[allow(clippy::upper_case_acronyms)]
pub struct BSSID(String);

impl BSSID {
    /// Assume we will always receive a correct bssid (for now)
    pub fn from_bytes_unchecked(v: Vec<u8>) -> Self {
        let mut str = String::new();
        for i in 0..v.len() {
            if let Some(b) = v.get(i) {
                if i == BSSID_LEN - 1 {
                    str.push_str(&format!("{:02x}", b));
                } else {
                    str.push_str(&format!("{:02x}:", b));
                }
            }
        }
        Self(str)
    }
}

impl Type for BSSID {
    fn signature() -> zvariant::Signature<'static> {
        zvariant::Signature::from_static_str_unchecked("ay")
    }
}

impl std::ops::Deref for BSSID {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Default)]
struct BSSIDVisitor {}

impl<'de> Visitor<'de> for BSSIDVisitor {
    type Value = BSSID;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "expecting a [u8] slice")
    }

    fn visit_byte_buf<E>(self, v: Vec<u8>) -> std::result::Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::Value::from_bytes_unchecked(v))
    }
}

impl<'de> Deserialize<'de> for BSSID {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let visitor = BSSIDVisitor::default();
        let val = deserializer.deserialize_byte_buf(visitor)?;
        Ok(val)
    }
}

impl fmt::Display for BSSID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.0)
    }
}

impl From<OwnedValue> for BSSID {
    fn from(v: OwnedValue) -> Self {
        Self::from_bytes_unchecked(<Vec<u8>>::try_from(v).unwrap())
    }
}

#[dbus_proxy(
    interface = "fi.w1.wpa_supplicant1.BSS",
    default_service = "fi.w1.wpa_supplicant1",
    gen_blocking = false,
    async_name = "WPASupplicantBSSProxy"
)]
trait WPASupplicantBSS {
    #[dbus_proxy(property, name = "SSID")]
    fn ssid(&self) -> zbus::Result<SSID>;

    #[dbus_proxy(property, name = "BSSID")]
    fn bssid(&self) -> zbus::Result<BSSID>;

    #[dbus_proxy(property)]
    fn signal(&self) -> zbus::Result<i16>;
}

impl WPASupplicantBSSProxy<'_> {
    /// The Mozilla location services advices to ignore the following access points
    /// - With a SSID that ends with `_nomap`
    /// - With unknown BSSID   
    pub async fn should_ignore(&self) -> Result<bool> {
        let bssid = self.bssid().await?;
        if bssid.is_empty() {
            tracing::debug!("Ignoring WiFi with unknown BSSID");
            return Ok(true);
        }
        let ssid = self.ssid().await?;
        if ssid.is_empty() || ssid.ends_with("_nomap") {
            tracing::debug!("Ignoring WiFi {bssid} with missing SSID or has '_nomap' suffix");
            return Ok(true);
        }
        Ok(false)
    }
}

#[cfg(test)]
mod tests {
    use super::{BSSID, SSID};

    #[test]
    fn test_ssid_to_string() {
        let ssid = SSID::from_bytes_unchecked(vec![
            82, 101, 100, 32, 72, 97, 116, 32, 71, 117, 101, 115, 116,
        ]);
        assert_eq!(*ssid, "Red Hat Guest");
        let ssid = SSID::from_bytes_unchecked(vec![]);
        assert_eq!(*ssid, "");
    }

    #[test]
    fn test_bssid_to_string() {
        let bssid = BSSID::from_bytes_unchecked(vec![0x12, 0x34, 0x56, 0xAB, 0xCD, 0xEF]);
        assert_eq!(*bssid, "12:34:56:ab:cd:ef");
        let bssid = BSSID::from_bytes_unchecked(vec![0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
        assert_eq!(*bssid, "ff:ff:ff:ff:ff:ff");
        let bssid = BSSID::from_bytes_unchecked(vec![0x12, 0x34, 0x56, 0x78, 0x90, 0xAB]);
        assert_eq!(*bssid, "12:34:56:78:90:ab");
    }
}
