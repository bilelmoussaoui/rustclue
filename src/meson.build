# Make sure we have the rust toolchain install and configure
# it for the build process
cargo = find_program('cargo')
cargo_options = [ '--manifest-path', meson.project_source_root() / 'Cargo.toml' ]
cargo_options += [ '--target-dir', meson.project_build_root() / 'target' ]
cargo_options += [ '--release' ]
rust_target = 'release'
cargo_env = [ 'CARGO_HOME=' + meson.project_build_root() / 'cargo-home' ]

# Work out the name of the project,
# based of the name of the meson project and it will add
# .exe if it is windows
#
# Then again... if you're running geoclue on windows you have
# far bigger issues ;)
outputname = meson.project_name()
if host_machine.system() == 'windows'
  outputname = '@0@.exe'.format(outputname)
endif

conf = configuration_data()
conf.set('sysconfdir', get_option('sysconfdir'))

# Configure the meson-config file
configure_file(
    input: 'mesonconfig.rs.in',
    output: 'mesonconfig.rs',
    configuration: conf,
)
run_command(
  'cp',
  join_paths(meson.project_build_root(), 'src', 'mesonconfig.rs'),
  join_paths(meson.project_source_root(), 'src', 'mesonconfig.rs'),
  check: true
)

# Compile the program
cargo_release = custom_target(
  'cargo-build',
  build_by_default: true,
  build_always_stale: true,
  output: outputname,
  console: true,
  install: true,
  install_dir: get_option('libexecdir'),
  command: [
    'env',
    cargo_env,
    cargo, 'build',
    cargo_options,
    '&&',
    'cp', 'target' / rust_target / meson.project_name(), '@OUTPUT@',
  ]
)
